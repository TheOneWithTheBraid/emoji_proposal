// ignore_for_file: avoid_print

import 'package:emoji_proposal/emoji_proposal.dart';

const englishMessage = 'We all love alpacas!';

void main(List<String> args) {
  final englishProposal = proposeEmojis(englishMessage,
      number: 5,
      languageCodes: {
        EmojiProposalLanguageCodes.en,
        EmojiProposalLanguageCodes.de
      });
  print("Proposed Emojis: $englishProposal");
}
