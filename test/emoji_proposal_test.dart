import 'package:emojis/emoji.dart';
import 'package:emojis/emojis.dart';
import 'package:test/test.dart';

import 'package:emoji_proposal/emoji_proposal.dart';

const englishMessage = 'We all love alpacas!';

void main() {
  test('Propose a predefined message', () {
    final result = proposeEmojis(englishMessage);
    expect(
        result.contains(Emoji.byChar(Emojis.smilingFaceWithHeartEyes)), isTrue);
    expect(result.contains(Emoji.byChar(Emojis.rainbow)), isTrue);
  });
}
