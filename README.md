# emoji_proposal

Use keyword-based text analysis in order to get emojis proposed.

## Features

If you have any application with no smart, AI, blockchain or serverless features, just add this package and play buzzword bingo with your cutomers: "We use AI-powered features in our application." - "For what?" - "To recommend emote reactions to text messages."

Why? Why not.

## Languages

Currently supported languages can be found in the `EmojiProposalLanguageCodes` enum.

As of now, this is English, Intalian, French, German. Emoji-characters within the `message` are analyzed in any case.

## Usage

This package exposes the `proposeEmojis` top-level function taking a (human-language) sentence, the number of proposals and the languages to check in as arguments.

```dart
const englishMessage = 'We all love alpacas!';

void main(List<String> args) {
  final englishProposal = proposeEmojis(englishMessage,
      number: 5,
      languageCodes: {
        EmojiProposalLanguageCodes.en,
        EmojiProposalLanguageCodes.de
      });
  print("Proposed Emojis: $englishProposal");
}
```

## Additional information

This package is dual-licensed under the terms and conditions of the [EUPL-1.2](LICENSE) *or* the [Apache 2](https://www.apache.org/licenses/LICENSE-2.0.txt) license.
